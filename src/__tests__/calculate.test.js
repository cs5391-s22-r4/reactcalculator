import React from 'react';
import calculate from "../logic/calculate";


describe('Calculate Tests', ()=>{
    test('Number button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: null},"4")).toHaveProperty('current', "4");
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"1")).toHaveProperty('current', "11");
        expect(calculate({
                previous: null,
                current: null,
                operation: "+"},"1")).toHaveProperty('current', "1");
    });

    test('Addition button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"+")).toHaveProperty('current', null);

    });

    test('Equal button tests', () => {
        expect(calculate({
                previous: 2,
                current: 2,
                operation: "+"},"=")).toHaveProperty('previous', "4");
        expect(calculate({
                previous: 2,
                current: 21,
                operation: "+"},"=")).toHaveProperty('previous', "23");
    });

    test('Percent button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"%")).toHaveProperty('current', "0.01");

        expect(calculate({
                previous: null,
                current: 11,
                operation: null},"%")).toHaveProperty('current', "0.11");

        expect(calculate({
                previous: null,
                current: 21,
                operation: null},"%")).toHaveProperty('current', "0.21");

        expect(calculate({
                previous: null,
                current: 31,
                operation: null},"%")).toHaveProperty('current', "0.31");

    });

    test('Addition button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: "+"},"1")).toHaveProperty('current', "1");

        expect(calculate({
                previous: null,
                current: 1,
                operation: "+"},"1")).toHaveProperty('current', "11");
        expect(calculate({
                previous: null,
                current: 2,
                operation: "+"},"1")).toHaveProperty('current', "21");
        expect(calculate({
                previous: null,
                current: 3,
                operation: "+"},"1")).toHaveProperty('current', "31");
        expect(calculate({
                previous: null,
                current: 4,
                operation: "+"},"1")).toHaveProperty('current', "41");
        expect(calculate({
                previous: null,
                current: 5,
                operation: "+"},"1")).toHaveProperty('current', "51");

    });

    test('Subtraction button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: "-"},"1")).toHaveProperty('current', "1");
        expect(calculate({
                previous: null,
                current: null,
                operation: "-"},"2")).toHaveProperty('current', "2");
        expect(calculate({
                previous: null,
                current: null,
                operation: "-"},"3")).toHaveProperty('current', "3");
        expect(calculate({
                previous: null,
                current: null,
                operation: "-"},"4")).toHaveProperty('current', "4");

    });

test('Decimal point button tests', () => {
        expect(calculate({
                previous: 1.1,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");
});

test('Decimal point button tests', () => {
        expect(calculate({
                previous: 2.1,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");
});

test('Decimal point button tests', () => {
        expect(calculate({
                previous: 3.1,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");
});

test('Decimal point button tests', () => {
        expect(calculate({
                previous: 4.1,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");


    });
});


test('Decimal point button tests', () => {
    expect(calculate({
        previous: 1.1,
        current: null,
        operation: null
    }, ".")).toHaveProperty('current', "0.");
});


test('Decimal point button tests', () => {
    expect(calculate({
        previous: 2.1,
        current: null,
        operation: null
    }, ".")).toHaveProperty('current', "0.");
});


test('Decimal point button tests', () => {
    expect(calculate({
        previous: 3.1,
        current: null,
        operation: null
    }, ".")).toHaveProperty('current', "0.");
});


test('Decimal point button tests', () => {
    expect(calculate({
        previous: 4.1,
        current: null,
        operation: null
    }, ".")).toHaveProperty('current', "0.");

}); 

