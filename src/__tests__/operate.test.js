import React from 'react';
import operate from "../logic/operate";
import Big from "big.js";



describe('Operate Multiplication Tests', () => {
    test('4 x 3 = 12 test', () => {
        expect(operate("4", "3", "x")).toBe("12");
    });
    test('16.2 x 6 = 97.2 test', () => {
        expect(operate("16.2", "6", "x")).toBe("97.2");
    });


});

describe('Operate Addition Tests', () => {
    test('4+-5 = -1 test', () => {
        expect(operate("4", "-5", "+")).toBe("-1");
    });

    test('11+-22 = -22 test', () => {
        expect(operate("11", "-22", "+")).toBe("-11");
    });
});



describe('Operate Subtraction Tests', () => {
    test('12-15 = -3 test', () => {
        expect(operate("12", "15", "-")).toBe("-3");
    });

    test('75-100 = -25 test', () => {
        expect(operate("75", "100", "-")).toBe("-25");
    });
});




describe('Operate Division Tests', () => {
    test('4/3 = 1.333 test', () => {
        expect(parseFloat(operate("4", "3", "÷"))).
            toBeCloseTo(1.3333333333, 5);
        // parseFloat() is a string-to-float function in Javascript.   
        // Try different precisions and string lengths to see what the test does.
    });
    test('20/5 = 4 test', () => {
        expect(operate("20", "5", "÷")).toBe("4");
    });

});


